﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DBDevel.Web.Models
{
    public class ConsoleMessage
    {
        public string Message
        {
            get; set;
        }

        public DateTime Time
        {
            get; set;
        }

        public enum MessageTypes
        {
            Message,
            Handshake
        }

        public MessageTypes MessageType
        {
            get; set;
        }
        public double Progress { get; internal set; }
    }
}