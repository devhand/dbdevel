﻿
api.addController('products', '/api/products/{productId}', {
    create: { method: 'post', path: '' },
    save: { method: 'put', path: '' },
    get: { method: 'get', path: '' },

    createInstance: { method: 'post', path: '/instance' },
    addScript: { method: 'post', path: '/script' }
});

api.addController('instances', '/api/instances/{instanceId}', {
    run: { method: 'post', path: '/run' },
    console: { method: 'post', path: '/console' },
});