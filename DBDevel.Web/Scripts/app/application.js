﻿
function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

var templateFromUrlLoader = {
    loadTemplate: function (name, templateConfig, callback) {
        if (templateConfig.view) {
            var viewName = replaceAll(templateConfig.view, '/', '_');

            if ($('#' + viewName).length > 0) {
                ko.components.defaultLoader.loadTemplate(name, $('#' + viewName).html(), callback);
            } else {
                // Uses jQuery's ajax facility to load the markup from a file
                var fullUrl = '/template/get?name=' + replaceAll(viewName, '_', '/') + '&cacheAge=' + templateConfig.maxCacheAge;
                $.get(fullUrl, function (markupString) {
                    // We need an array of DOM nodes, not a string.
                    // We can use the default loader to convert to the
                    // required format.
                    $('body').append('<script type="text/template" id="' + viewName + '">' + markupString + '</script>');

                    ko.components.defaultLoader.loadTemplate(name, markupString, callback);
                });
            }
        } else {
            // Unrecognized config format. Let another loader handle it.
            callback(null);
        }
    }
};

// Register it
ko.components.loaders.unshift(templateFromUrlLoader);

function ApplicationViewModel() {
    var me = this;

    me.PageComponentName = ko.observable();
    me.Route = ko.observable();

    me.Context = {
        Product: ko.observable(),
        Instance: ko.observable(),

        Clear: function () {
            me.Context.Product(null);
            me.Context.Instance(null);
        }
    }

    me.ShowPage = function (action, controller, routeParams) {
        console.log('setting route: ' + controller + '/' + action);
        me.PageComponentName(controller + '/' + action);
        me.Route(routeParams);
    }
}

$applicationViewModel = new ApplicationViewModel();

$(function () {
    ko.applyBindings($applicationViewModel)

    router.run('#/');
});