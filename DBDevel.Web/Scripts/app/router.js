﻿
var router = Sammy(function () {

    this.get('#/', function () {
        $applicationViewModel.ShowPage('overview', 'overview', this);
    });

    this.get('#/products/:id/script', function () {
        $applicationViewModel.ShowPage('addscript', 'products', this);
    });

    this.get('#/products/:id/instance', function () {
        $applicationViewModel.ShowPage('createinstance', 'products', this);
    });

    this.get('#/products/create', function () {
        $applicationViewModel.ShowPage('create', 'products', this);
    });

    this.get('#/products/:id', function () {
        $applicationViewModel.ShowPage('detail', 'products', this);
    });

    this.get('#/products', function () {
        $applicationViewModel.ShowPage('all', 'products', this);
    });

    this.get('#/instances/:id/run', function () {
        $applicationViewModel.ShowPage('run', 'instances', this);
    });
});