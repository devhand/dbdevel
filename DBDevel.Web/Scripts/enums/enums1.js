﻿window.Enums = {
	KnockoutJSTypes: {
			Observable: 0,
			ComputedObservable: 1,
			ObservableArray: 2,
	},
	ScriptStates: {
			New: 0,
			Applied: 1,
			Applying: 2,
			Error: 3,
	},
}

window.IdTextEnums = {
	KnockoutJSTypes: [
{
			Key: 'Observable',
			Id: 0,
			Text: 'Observable'
},
{
			Key: 'ComputedObservable',
			Id: 1,
			Text: 'ComputedObservable'
},
{
			Key: 'ObservableArray',
			Id: 2,
			Text: 'ObservableArray'
},
	],
	ScriptStates: [
{
			Key: 'New',
			Id: 0,
			Text: 'New'
},
{
			Key: 'Applied',
			Id: 1,
			Text: 'Applied'
},
{
			Key: 'Applying',
			Id: 2,
			Text: 'Applying'
},
{
			Key: 'Error',
			Id: 3,
			Text: 'Error'
},
	],
}