﻿
ChangeScriptVM.prototype.isAppliedInInstance = function (instanceId) {
    var me = this;

    var res = Lazy(me.Instances()).any(function (i) {
        return i.InstanceId() == instanceId;
    });

    return res;
}

ChangeScriptVM.prototype.getState = function (instanceId) {
    var me = this;

    var res = Lazy(me.Instances()).where(function (i) {
        return i.InstanceId() == instanceId;
    }).first();

    if (res == null) return -1;

    return res.State();
}