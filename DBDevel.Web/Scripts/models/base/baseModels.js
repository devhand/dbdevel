﻿		function ChangeScriptInstanceVM(parentVm, initObj) {
			var me = this;

			me.ParentViewModel = parentVm;

			var mappingOptions = {};

								me.InstanceId = ko.observable();
									me.ChangeScriptId = ko.observable();
									me.ChangeScript = ko.observable();
									me.State = ko.observable();
				
			if (initObj) {
				ko.mapping.fromJS(initObj, mappingOptions, me);
			}

			me.toJS = function() {
				var oldParent = me.ParentViewModel;

				me.ParentViewModel = null;

				var result = ko.toJS(me);

				me.ParentViewModel = oldParent;

				return result;
			}
		}
				function ChangeScriptVM(parentVm, initObj) {
			var me = this;

			me.ParentViewModel = parentVm;

			var mappingOptions = {};

								me.Id = ko.observable();
									me.ProductId = ko.observable();
									me.Script = ko.observable();
									me.Notes = ko.observable();
									me.Created = ko.observable();
									me.AuthorId = ko.observable();
									me.Instances = ko.observableArray();

					mappingOptions.Instances = { create: function(options) { return new ChangeScriptInstanceVM(me, options.data); } }
				
			if (initObj) {
				ko.mapping.fromJS(initObj, mappingOptions, me);
			}

			me.toJS = function() {
				var oldParent = me.ParentViewModel;

				me.ParentViewModel = null;

				var result = ko.toJS(me);

				me.ParentViewModel = oldParent;

				return result;
			}
		}
				function InstanceVM(parentVm, initObj) {
			var me = this;

			me.ParentViewModel = parentVm;

			var mappingOptions = {};

								me.Id = ko.observable();
									me.ProductId = ko.observable();
									me.Name = ko.observable();
									me.Server = ko.observable();
									me.Database = ko.observable();
									me.Username = ko.observable();
									me.Password = ko.observable();
									me.UseWinAuth = ko.observable();
									me.IncludedInChangeScript = ko.observable();
									me.ChangeScripts = ko.observableArray();

					mappingOptions.ChangeScripts = { create: function(options) { return new ChangeScriptInstanceVM(me, options.data); } }
				
			if (initObj) {
				ko.mapping.fromJS(initObj, mappingOptions, me);
			}

			me.toJS = function() {
				var oldParent = me.ParentViewModel;

				me.ParentViewModel = null;

				var result = ko.toJS(me);

				me.ParentViewModel = oldParent;

				return result;
			}
		}
				function ProductVM(parentVm, initObj) {
			var me = this;

			me.ParentViewModel = parentVm;

			var mappingOptions = {};

								me.Id = ko.observable();
									me.Instances = ko.observableArray();

					mappingOptions.Instances = { create: function(options) { return new InstanceVM(me, options.data); } }
									me.Name = ko.observable();
									me.ChangeScripts = ko.observableArray();

					mappingOptions.ChangeScripts = { create: function(options) { return new ChangeScriptVM(me, options.data); } }
				
			if (initObj) {
				ko.mapping.fromJS(initObj, mappingOptions, me);
			}

			me.toJS = function() {
				var oldParent = me.ParentViewModel;

				me.ParentViewModel = null;

				var result = ko.toJS(me);

				me.ParentViewModel = oldParent;

				return result;
			}
		}
				function ScriptLogVM(parentVm, initObj) {
			var me = this;

			me.ParentViewModel = parentVm;

			var mappingOptions = {};

								me.Id = ko.observable();
									me.ScriptId = ko.observable();
									me.Message = ko.observable();
									me.Datetime = ko.observable();
									me.InstanceId = ko.observable();
									me.LogType = ko.observable();
				
			if (initObj) {
				ko.mapping.fromJS(initObj, mappingOptions, me);
			}

			me.toJS = function() {
				var oldParent = me.ParentViewModel;

				me.ParentViewModel = null;

				var result = ko.toJS(me);

				me.ParentViewModel = oldParent;

				return result;
			}
		}
				function UserVM(parentVm, initObj) {
			var me = this;

			me.ParentViewModel = parentVm;

			var mappingOptions = {};

								me.Id = ko.observable();
									me.Firstname = ko.observable();
									me.Test = ko.observableArray();

					mappingOptions.Test = { create: function(options) { return new Int32(me, options.data); } }
				
			if (initObj) {
				ko.mapping.fromJS(initObj, mappingOptions, me);
			}

			me.toJS = function() {
				var oldParent = me.ParentViewModel;

				me.ParentViewModel = null;

				var result = ko.toJS(me);

				me.ParentViewModel = oldParent;

				return result;
			}
		}
		