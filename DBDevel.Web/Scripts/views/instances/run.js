﻿
ko.components.register('instances/run', {
    viewModel: function (params) {
        var me = this;

        me.Log = ko.observableArray();

        me.Progress = ko.observable(0);

        var source = new EventSource('/api/instances/' + params.route.params.id + '/console?subscribe=true');

        source.onopen = function (e) {
            api.instances.run({ instanceId: params.route.params.id }, {}, function (data) {

            });
        }

        source.onmessage = function (e) {
            try {
                var data = JSON.parse(e.data);
                console.log(data);
                me.Log.push(new ScriptLogVM(me, { Message: data.Message }));

                me.Progress(data.Progress);
            } catch (e) {

            }
        }

        source.onerror = function(event) {
            console.log(event.message);
        }

        me.dispose = function () {
            console.log('disposing');
            source.close();
        }
    },
    template: { view: '/instances/run' }
});