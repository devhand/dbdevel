﻿
ko.components.register('products/create', {
    viewModel: function (params) {
        var me = this;

        me.Model = ko.observable(new ProductVM(me));

        me.submit = function () {
            api.products.create({}, me.Model().toJS(), function (data) {
                router.setLocation('#/products');
            });
        }
    },
    template: { view: '/products/create' }
});