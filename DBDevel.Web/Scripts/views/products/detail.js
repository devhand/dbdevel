﻿
ko.components.register('products/detail', {
    viewModel: function (params) {
        var me = this;

        me.Model = ko.observable(new ProductVM(me));
        
        api.products.get({ productId: params.route.params.id }, {}, function (data) {
            me.Model(new ProductVM(me, data));

            $applicationViewModel.Context.Product(me.Model());
        });
    },
    template: { view: '/products/detail' }
});