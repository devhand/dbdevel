﻿
ko.components.register('products/all', {
    viewModel: function (params) {
        var me = this;

        $applicationViewModel.Context.Clear();

        me.Products = ko.observableArray();

        api.products.get({}, {}, function (data) {
            me.Products.removeAll();

            for (var i in data) {
                me.Products.push(new ProductVM(me, data[i]));
            }
        });
    },
    template: { view: '/products/all' }
});