﻿
ko.components.register('products/createinstance', {
    viewModel: function (params) {
        var me = this;

        me.Model = ko.observable(new InstanceVM(me));

        me.submit = function () {
            api.products.createInstance({ productId: params.route.params.id }, me.Model().toJS(), function (data) {
                router.setLocation('#/products/' + params.route.params.id);
            });
        }
    },
    template: { view: '/products/createinstance' }
});