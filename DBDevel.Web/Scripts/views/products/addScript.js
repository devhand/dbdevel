﻿
ko.components.register('products/addscript', {
    viewModel: function (params) {
        var me = this;

        me.Product = ko.observable(new ProductVM(me));
        me.Model = ko.observable(new ChangeScriptVM(me));

        me.IncludeAllInstances = ko.observable();
        me.IncludeAllInstances.subscribe(function (val) {
            Lazy(me.Product().Instances()).forEach(function (i) { i.IncludedInChangeScript(val) });
        });

        api.products.get({ productId: params.route.params.id }, {}, function (data) {
            me.Product(new ProductVM(me, data));

            me.IncludeAllInstances(true);

            $applicationViewModel.Context.Product(me.Product());
        });

        me.submit = function () {
            var cs = me.Model();

            var includeIn = Lazy(me.Product().Instances()).where(function (i) { return i.IncludedInChangeScript() }).toArray();

            me.Model().Instances.removeAll();
            Lazy(includeIn).forEach(function (i) { me.Model().Instances.push({ InstanceId: i.Id() }) });

            api.products.addScript({ productId: params.route.params.id }, me.Model().toJS(), function () {
                router.setLocation('#/products/' + params.route.params.id + '');
            });
        }
    },
    template: { view: '/products/addscript' }
});