﻿/**
*
*
**/
function ApiWrapper() {
    var me = this;

    me._apiCall = function (url, method, data, success, fail) {
        console.log('Ajax call: ' + url + ', ' + method);

        var dataObj = !$.isEmptyObject(data) ? JSON.stringify(data) : '';

        $.ajax({
            url: url,
            type: method.toUpperCase(),
            dataType: 'json',
            contentType: 'application/json',
            data: dataObj,
            success: function (response) {
                success ? success(response) : '';
            },
            error: function (response) {
                if (response.status == 401) window.location.href = '/account/signin';
                if (fail) {
                    fail(response);
                } else {
                    $loader.ShowFullPageError();
                }
            }
        });
    }

    me._createUrl = function (basePath, path, urlParams) {
        var fullPath = basePath + path;
        var regExp = /\{([^\}]+)\}/g;
        var matches = fullPath.match(regExp);
        var finalPath = '';

        var usedParamsInPath = [];

        //urlParams['organizationUri'] = OrganizationUri;

        for (var i in matches) {
            var paramName = matches[i].substring(1, matches[i].length - 1);
            fullPath = fullPath.replace('{' + paramName + '}', urlParams[paramName] ? urlParams[paramName] : '');
            fullPath = fullPath.replace('//', '/');

            if (urlParams[paramName]) usedParamsInPath.push(urlParams[paramName]);
        }

        var questMark = false;
        for (var i in urlParams) {
            if ($.inArray(urlParams[i], usedParamsInPath) == -1) {
                if (questMark == false) {
                    fullPath += '?';
                    questMark = true;
                } else {
                    fullPath += '&';
                }

                fullPath += i + '=' + urlParams[i];
            }
        }

        return fullPath;
    }

    me._createHandler = function (i, calls, basePath) {
        "use strict";
        return function (urlParams, bodyParams, success, fail) {
            me._apiCall(me._createUrl(basePath, calls[i].path, urlParams), calls[i].method, bodyParams, success, fail);
        }
    }

    me.addController = function (controllerName, basePath, calls) {
        me[controllerName] = {};
        for (var i in calls) {
            me[controllerName][i] = me._createHandler(i, calls, basePath);
        }
    }
}
var api = new ApiWrapper();