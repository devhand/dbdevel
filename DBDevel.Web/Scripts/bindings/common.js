﻿
ko.bindingHandlers.showPage = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var $el = $(element);

        var config = ko.unwrap(valueAccessor());

        $el.on('click.showPage', function () {
            //$applicationViewModel.ShowPage(ko.unwrap(config.action), ko.unwrap(config.controller))
            window.location = '/' + ko.unwrap(config.controller) + '/' + ko.unwrap(config.action);
        });

        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {

        });
    }
};

ko.bindingHandlers.navigate = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var $el = $(element);

        var config = valueAccessor();

        var subscribers = [];
        
        for (var i in config) {
            if (i == 'url') continue;

            var item = config[i];

            if (ko.isObservable(item)) {
                subscribers.push(item.subscribe(function (val) {
                    $el.off('click.navigatebinding');

                    createHandler(valueAccessor());
                }));
            }
        }

        function createHandler(config) {

            $el.on('click.navigatebinding', function (e) {
                var unwrappedConfig = ko.toJS(config);

                delete unwrappedConfig.url;
                delete unwrappedConfig.changeTrigger;

                var url = api._createUrl('', config.url, unwrappedConfig);

                if (allBindings.get('debug')) {
                    console.log(url);
                } else {
                    window.location = url;
                }

                e.preventDefault();
            });
        }
        createHandler(config);

        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $el.off('click.navigatebinding');

            for (var i in subscribers) {
                subscribers[i].dispose();
            }
        });
    }
};

ko.bindingHandlers.console = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        console.log(valueAccessor());
    }
};