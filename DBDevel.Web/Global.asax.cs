﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Web.Optimization;

namespace DBDevel.Web
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var scriptsExternalBundle = new ScriptBundle("~/externals");

            scriptsExternalBundle.Include(
                "~/Scripts/jquery-1.10.2.js",
                "~/Scripts/knockout-3.4.0.debug.js",
                "~/Scripts/knockout.mapping-latest.debug.js",
                "~/Scripts/sammy-0.7.5.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/lazy.js",
                "~/Scripts/devhand.api.js"
                );

            var scriptsAppBundle = new ScriptBundle("~/app");

            scriptsAppBundle.Include(
                "~/Scripts/enums/enums1.js",
                "~/Scripts/models/base/baseModels.js",
                "~/Scripts/app/application.js",
                "~/Scripts/app/api.js",
                "~/Scripts/app/router.js",
                "~/Scripts/bindings/common.js"
                );

            var scriptsAppExBundle = new ScriptBundle("~/appex");

            scriptsAppExBundle.IncludeDirectory("~/Scripts/enums", "*.js", true);
            scriptsAppExBundle.IncludeDirectory("~/Scripts/models", "*.js", false);
            scriptsAppExBundle.IncludeDirectory("~/Scripts/views", "*.js", true);

            BundleTable.Bundles.Add(scriptsExternalBundle);
            BundleTable.Bundles.Add(scriptsAppBundle);
            BundleTable.Bundles.Add(scriptsAppExBundle);
        }
    }
}