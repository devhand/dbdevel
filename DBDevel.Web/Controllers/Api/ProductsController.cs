﻿using DBDevel.Business;
using DBDevel.Business.ViewModels;
using DBDevel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DBDevel.Web.Controllers.Api
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("api/products")]
    [Route("{productId?}")]
    public class ProductsController : ApiController
    {
        public ProductsService ProductsService
        {
            get; set;
        } = new ProductsService();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public PagingList<ProductVM> GetAll([FromUri]int page = 1)
        {
            return this.ProductsService.GetProducts(page);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{productId?}")]
        public ProductVM Get(int productId)
        {
            return this.ProductsService.Get(productId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        [HttpPost]
        [Route("")]
        public void Post(ProductVM product)
        {
            this.ProductsService.Create(product);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("{productId?}/instance")]
        public void PostInstance([FromUri]int productId, InstanceVM instance)
        {
            instance.ProductId = productId;

            this.ProductsService.CreateInstance(instance);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="instance"></param>
        [HttpPost]
        [Route("{productId?}/script")]
        public void PostScript([FromUri]int productId, ChangeScriptVM changeScript)
        {
            changeScript.ProductId = productId;
            changeScript.AuthorId = 1; //TODO

            this.ProductsService.AddScript(changeScript);
        }
    }
}
