﻿using DBDevel.Business;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using DBDevel.Business.Extensions;

namespace DBDevel.Web.Controllers.Api
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("api/instances")]
    [Route("{instanceId?}")]
    public class InstancesController : ApiController
    {
        public InstancesService InstancesService
        {
            get; set;
        } = new InstancesService();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instanceId"></param>
        [HttpPost]
        [Route("{instanceId?}/run")]
        public void Run(int instanceId)
        {
            this.WriteToConsole("Starting...", 0);

            try
            {
                this.InstancesService.ApplyScripts(instanceId,
                    (result, progress) =>
                    {
                        if (result.ResultType == Business.Sql.SqlResultTypes.Ok)
                        {
                            this.WriteToConsole($"Applied, affected rows: {result.AffectedRows}", progress);
                        }
                        else if (result.ResultType == Business.Sql.SqlResultTypes.Info)
                        {
                            this.WriteToConsole(result.Message, progress);
                        }
                        else if (result.ResultType == Business.Sql.SqlResultTypes.Error)
                        {
                            this.WriteToConsole($"Executing error.", progress);
                            this.WriteToConsole(result.Message, progress);
                        }
                    });
            }
            catch (Exception e)
            {
                this.WriteToConsole("General error.", 0);
                this.WriteToConsole(e.FormatAllMessages(), 0);
            }

            this.WriteToConsole("Done", 100);            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{instanceId?}/console")]
        public HttpResponseMessage Console(HttpRequestMessage request)
        {
            HttpResponseMessage response = request.CreateResponse();
            response.Content = new PushStreamContent((stream, headers, ctx) => OnStreamAvailable(stream, headers, ctx), "text/event-stream");
            return response;
        }

        private static readonly ConcurrentQueue<StreamWriter> clientStreams = new ConcurrentQueue<StreamWriter>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="headers"></param>
        /// <param name="context"></param>
        private static void OnStreamAvailable(Stream stream, HttpContent headers, TransportContext context)
        {
            StreamWriter streamwriter = new StreamWriter(stream, Encoding.UTF8);

            streamwriter.AutoFlush = true;

            var clientId = Guid.NewGuid().ToString();

            clientStreams.Enqueue(streamwriter);

            //streamwriter.Write("data: \n\n");

            //streamwriter.WriteLine(JsonConvert.SerializeObject(new Models.ConsoleMessage()
            //{
            //    Message = clientId.ToString(),
            //    MessageType = Models.ConsoleMessage.MessageTypes.Handshake,
            //    Time = DateTime.Now
            //}));
            //streamwriter.Flush();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        private void WriteToConsole(string message, double progress)
        {
            foreach (var client in clientStreams.ToList())
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(message))
                    {
                        client.Write("\n\n\n\n");
                    }
                    else
                    {
                        client.Write("data: " + JsonConvert.SerializeObject(new Models.ConsoleMessage()
                        {
                            Message = message,
                            Progress = progress,
                            MessageType = Models.ConsoleMessage.MessageTypes.Message,
                            Time = DateTime.Now
                        }) + "\n\n");
                        client.Write("\n\n");
                    }
                }
                catch
                {
                    StreamWriter sw;
                    clientStreams.TryDequeue(out sw);
                    sw.BaseStream.Dispose();
                    sw.Dispose();
                }
            }
        }
    }
}
