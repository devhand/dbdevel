﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DBDevel.Web.Controllers
{
    public class TemplateController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Get(string name)
        {
            return PartialView("~/Views" + name + ".cshtml");
        }
    }
}