﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Business.Extensions
{
    public static class StringBuilderExtensions
    {
        public static void WriteLineWithIndent(this StringBuilder sb, int indents, string text)
        {
            string indent = "   ";

            for (var i = 0; i < indents; i++)
            {
                sb.Append(indent);
            }

            sb.AppendLine(text);
        }
    }
}
