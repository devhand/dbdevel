﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Business.Extensions
{
    public static class TypeExtensions
    {
        public static Enums.KnockoutJSTypes GetKnockoutJSType(this System.Reflection.PropertyInfo pi)
        {
            if (pi.PropertyType != typeof(string))
            {
                if (typeof(IEnumerable).IsAssignableFrom(pi.PropertyType) || typeof(ICollection).IsAssignableFrom(pi.PropertyType))
                {
                    return Enums.KnockoutJSTypes.ObservableArray;
                }
            }

            if (pi.GetSetMethod() == null)
            {
                return Enums.KnockoutJSTypes.ComputedObservable;
            }

            return Enums.KnockoutJSTypes.Observable;
        }
    }
}
