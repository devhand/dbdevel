﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Business.Extensions
{
    public static class ExceptionExtensions
    {
        public static string FormatAllMessages(this Exception ex)
        {
            var e = ex;

            StringBuilder sb = new StringBuilder();

            int cycle = 0;

            while (e != null)
            {
                sb.WriteLineWithIndent(cycle, e.Message);
                sb.AppendLine();

                e = e.InnerException;
                cycle++;
            }

            return sb.ToString();
        }
    }
}
