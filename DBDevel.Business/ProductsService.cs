﻿using DBDevel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBDevel.Business.ViewModels;

namespace DBDevel.Business
{
    /// <summary>
    /// 
    /// </summary>
    public class ProductsService : Service
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public PagingList<ViewModels.ProductVM> GetProducts(int page)
        {
            return this.PagedResult(this.Entities.Products.OrderBy(i => i.Name).ToModels(), page);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public ProductVM Create(ProductVM product)
        {
            var entity = new Data.Product();

            entity.IsActive = true;
            entity.Name = product.Name;

            this.Entities.Products.Add(entity);

            this.Entities.SaveChanges();

            product.Id = entity.Id;

            return product;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProductVM Get(int id)
        {
            return this.Entities.Products.Where(i => i.Id == id).First().ToModel();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public InstanceVM CreateInstance(InstanceVM instance)
        {
            var entity = new Data.Instance();

            entity.IsActive = true;
            entity.Name = instance.Name;
            entity.ProductId = instance.ProductId;
            entity.Server = instance.Server;
            entity.Database = instance.Database;
            entity.Username = instance.Username;
            entity.Password = instance.Password;
            entity.UseWinAuth = instance.UseWinAuth;

            this.Entities.Instances.Add(entity);

            this.Entities.SaveChanges();

            instance.Id = entity.Id;

            return instance;            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        public ChangeScriptVM AddScript(ChangeScriptVM script)
        {
            var entity = new Data.ChangeScript();

            entity.Created = DateTime.Now;
            entity.ProductId = script.ProductId;
            entity.Script = script.Script;
            entity.Notes = script.Notes;
            entity.AuthorId = script.AuthorId;

            if (script.Instances != null && script.Instances.Any())
            {
                var instanceIds = script.Instances.Select(i => i.InstanceId);

                foreach (var instanceId in instanceIds)
                {
                    entity.ChangeScriptsInstances.Add(new Data.ChangeScriptsInstance()
                    {
                        InstanceId = instanceId,
                        State = Data.ScriptStates.New
                    });
                }
            }
            else
            {
                throw new ArgumentException("The script must be assigned in an instance.");
            }

            this.Entities.ChangeScripts.Add(entity);

            this.Entities.SaveChanges();

            script.Id = entity.Id;

            return script;
        }
    }
}
