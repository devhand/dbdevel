﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Business.Enums
{
    public enum KnockoutJSTypes
    {
        Observable,
        ComputedObservable,
        ObservableArray
    }
}
