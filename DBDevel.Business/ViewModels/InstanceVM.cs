﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Business.ViewModels
{
    public class InstanceVM : ViewModel
    {
        public int Id
        {
            get; set;
        }

        public int ProductId
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }

        public string Server
        {
            get; set;
        }

        public string Database
        {
            get; set;
        }

        public string Username
        {
            get; set;
        }

        public string Password
        {
            get; set;
        }

        public bool UseWinAuth
        {
            get; set;
        }

        public bool IncludedInChangeScript
        {
            get; set;
        }

        public IEnumerable<ChangeScriptInstanceVM> ChangeScripts
        {
            get; set;
        }
    }
}
