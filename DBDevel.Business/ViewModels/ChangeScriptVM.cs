﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBDevel.Business.Data;

namespace DBDevel.Business.ViewModels
{
    public class ChangeScriptVM : ViewModel
    {
        public int Id { get; set; }
        public Nullable<int> ProductId { get; set; }
        public string Script { get; set; }
        public string Notes { get; set; }
        public System.DateTime Created { get; set; }
        public int AuthorId { get; set; }

        public IEnumerable<ChangeScriptInstanceVM> Instances
        {
            get; set;
        }
    }
}
