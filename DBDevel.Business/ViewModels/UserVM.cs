﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Business.ViewModels
{
    public class UserVM : ViewModel
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public List<int> Test { get; set; }
    }
}
