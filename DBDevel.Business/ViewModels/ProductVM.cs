﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Business.ViewModels
{
    public class ProductVM : ViewModel
    {
        public int Id { get; set; }
        public IEnumerable<InstanceVM> Instances { get; internal set; }
        public string Name { get; set; }

        public IEnumerable<ChangeScriptVM> ChangeScripts
        {
            get; set;
        }
    }
}
