﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Business.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    static class Mapping
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static ProductVM ToModel(this Data.Product entity)
        {
            var vm = new ProductVM();

            vm.Id = entity.Id;
            vm.Name = entity.Name;
            vm.ChangeScripts = entity.ChangeScripts.AsQueryable().ToModels();

            vm.Instances = entity.Instances.AsQueryable().ToModels();

            //if (includeChangeScriptsFull)
            //{
            //    vm.ChangeScripts = entity.ChangeScripts.AsQueryable().ToModels(true);
            //}
            //else if (includeChangeScriptsMeta)
            //{
            //    vm.ChangeScripts = entity.ChangeScripts.AsQueryable().ToModels();
            //}

            return vm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<ProductVM> ToModels(this IQueryable<Data.Product> entities)
        {
            return entities.Select(i => new ProductVM()
            {
                Id = i.Id,
                Name = i.Name
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static ChangeScriptInstanceVM ToModel(this Data.ChangeScriptsInstance entity)
        {
            var vm = new ChangeScriptInstanceVM();

            vm.ChangeScriptId = entity.ChangeScriptId;
            vm.ChangeScript = entity.ChangeScript.ToModel(includeInstances: false);
            vm.InstanceId = entity.InstanceId;
            vm.State = entity.State;

            return vm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<ChangeScriptInstanceVM> ToModels(this IQueryable<Data.ChangeScriptsInstance> entities)
        {
            return entities.Select(i => new ChangeScriptInstanceVM()
            {
                ChangeScriptId = i.ChangeScriptId,
                ChangeScript = i.ChangeScript.ToModel(false),
                InstanceId = i.InstanceId,
                State = i.State
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static InstanceVM ToModel(this Data.Instance entity)
        {
            var vm = new InstanceVM();

            vm.Id = entity.Id;
            vm.Name = entity.Name;
            vm.ProductId = entity.ProductId;
            vm.Server = entity.Server;
            vm.Database = entity.Database;
            vm.Username = entity.Username;
            //vm.Password = entity.Password;
            vm.UseWinAuth = entity.UseWinAuth;

            return vm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<InstanceVM> ToModels(this IQueryable<Data.Instance> entities)
        {
            return entities.Select(i => new InstanceVM()
            {
                Id = i.Id,
                Name = i.Name,
                ProductId = i.ProductId,
                Database = i.Database,
                Server = i.Server,
                Username = i.Username,
                ChangeScripts = i.ChangeScriptsInstances.AsQueryable().ToModels()
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static ChangeScriptVM ToModel(this Data.ChangeScript entity, bool includeInstances = true)
        {
            var vm = new ChangeScriptVM();

            vm.Id = entity.Id;
            vm.Notes = entity.Notes;
            vm.ProductId = entity.ProductId;
            //vm.Script = entity.Script;
            vm.AuthorId = entity.AuthorId;
            vm.Created = entity.Created;

            if (includeInstances)
            {
                vm.Instances = entity.ChangeScriptsInstances.AsQueryable().ToModels();
            }

            return vm;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<ChangeScriptVM> ToModels(this IQueryable<Data.ChangeScript> entities, bool includeScript = false)
        {
            return entities.Select(i => new ChangeScriptVM()
            {
                Id = i.Id,
                Notes = i.Notes,
                ProductId = i.ProductId,
                AuthorId = i.AuthorId,
                Created = i.Created,
                Script = includeScript ? i.Script : "",
                Instances = i.ChangeScriptsInstances.AsQueryable().ToModels()
            });
        }
    }
}
