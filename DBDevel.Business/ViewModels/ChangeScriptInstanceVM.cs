﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Business.ViewModels
{
    public class ChangeScriptInstanceVM : ViewModel
    {
        public int InstanceId
        {
            get; set;
        }

        public int ChangeScriptId
        {
            get; set;
        }

        public ChangeScriptVM ChangeScript
        {
            get; set;
        }

        public Data.ScriptStates State
        {
            get; set;
        }
    }
}
