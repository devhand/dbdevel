﻿using DBDevel.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Business.ViewModels
{
    public class ScriptLogVM : ViewModel
    {
        public int Id { get; set; }
        public int ScriptId { get; set; }
        public string Message { get; set; }
        public System.DateTime Datetime { get; set; }
        public int InstanceId { get; set; }
        public ScriptLogTypes LogType { get; set; }
    }
}
