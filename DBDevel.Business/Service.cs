﻿using DBDevel.Business.ViewModels;
using DBDevel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Business
{
    public abstract class Service
    {
        private Data.DBDevelEntities1 entites = new Data.DBDevelEntities1();

        /// <summary>
        /// 
        /// </summary>
        protected Data.DBDevelEntities1 Entities
        {
            get
            {
                return this.entites;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<Type, List<System.Reflection.PropertyInfo>> GetViewModels()
        {
            var types = new List<Type>();

            foreach (var type in typeof(Service).Assembly.GetTypes().Where(i => i.IsSubclassOf(typeof(ViewModel))))
            {
                types.Add(type);
            }

            var resultTypes = new Dictionary<Type, List<System.Reflection.PropertyInfo>>();

            foreach (var type in types)
            {
                resultTypes.Add(type, new List<System.Reflection.PropertyInfo>());

                foreach (var propertyInfo in type.GetProperties().Where(i => !i.GetCustomAttributes(true).Where(y => y.GetType() == typeof(DontExportToJsAttribute)).Any()))
                {
                    resultTypes[type].Add(propertyInfo);
                }
            }

            return resultTypes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="items"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        protected PagingList<TModel> PagedResult<TModel>(IQueryable<TModel> items, int page, int itemsCountPerPage = 20)
        {
            return new PagingList<TModel>(items.Skip(itemsCountPerPage * (page - 1)).Take(itemsCountPerPage))
            {
                AllItemsCount = items.Count(),
                CurrentPage = page,
                PerPageCount = itemsCountPerPage
            };
        }
    }
}
