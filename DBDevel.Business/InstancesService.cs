﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBDevel.Business.Extensions;

namespace DBDevel.Business
{
    public class InstancesService : Service
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="instanceId"></param>
        /// <param name="updaterAction"></param>
        /// <returns></returns>
        public void ApplyScripts(int instanceId, Action<Sql.SqlResult, double> updaterAction)
        {
            var instance = this.Entities.Instances.Where(i => i.Id == instanceId).First();
            var scripts = this.Entities.ChangeScriptsInstances.Where(i => i.InstanceId == instanceId && i.State == Data.ScriptStates.New).OrderBy(i => i.ChangeScript.Created);

            double progress = 0.0d;
            var scriptsCount = scripts.Count();

            using (var connection = new SqlConnection(this.GetConnectionString(instance)))
            {
                try
                {
                    connection.Open();
                }
                catch (Exception e)
                {
                    updaterAction(new Sql.SqlResult()
                    {
                        Message = e.FormatAllMessages(),
                        ResultType = Sql.SqlResultTypes.Error
                    }, 0);

                    return;
                }

                int counter = 1;
                foreach (var script in scripts.ToList())
                {
                    Sql.SqlResult result = null;

                    updaterAction(new Sql.SqlResult()
                    {
                        Message = "Applying script " + script.ChangeScript.Notes + "...",
                        ResultType = Sql.SqlResultTypes.Info
                    }, progress);

                    try
                    {
                        result = this.RunScript(connection, script);
                    }
                    catch (Exception e)
                    {
                        script.State = Data.ScriptStates.Error;
                        this.Entities.SaveChanges();

                        updaterAction(new Sql.SqlResult()
                        {
                            Message = e.FormatAllMessages(),
                            ResultType = Sql.SqlResultTypes.Error
                        }, progress);

                        return;
                    }

                    progress = ((double)counter / (double)scriptsCount) * 100.0d;
                    counter++;

                    updaterAction(result, progress);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="script"></param>
        /// <returns></returns>
        protected Sql.SqlResult RunScript(SqlConnection connection, Data.ChangeScriptsInstance script)
        {
            script.State = Data.ScriptStates.Applying;
            this.Entities.SaveChanges();

            var result = new Sql.SqlResult();

            using (var command = new SqlCommand())
            {
                command.Connection = connection;
                command.CommandTimeout = 180;

                command.CommandText = script.ChangeScript.Script;

                result.AffectedRows = command.ExecuteNonQuery();

                result.ResultType = Sql.SqlResultTypes.Ok;
            }

            script.State = Data.ScriptStates.Applied;
            this.Entities.SaveChanges();

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        protected string GetConnectionString(Data.Instance instance)
        {
            if (instance.UseWinAuth)
            {
                return $"Server={instance.Server};Database={instance.Database};Integrated Security=true;";
            }
            else
            {
                return $"Server={instance.Server};Database={instance.Database};User Id={instance.Username};Password={instance.Password}";
            }
        }
    }
}
