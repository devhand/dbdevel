﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Business.Sql
{
    public class SqlResult
    {
        public int AffectedRows
        {
            get; set;
        }

        public string Message
        {
            get; set;
        }

        public SqlResultTypes ResultType
        {
            get; set;
        }
    }

    public enum SqlResultTypes
    {
        Ok,
        Error,
        Info
    }
}
