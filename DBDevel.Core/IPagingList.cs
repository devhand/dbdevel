﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Core
{
    public interface IPagingList
    {
        /// <summary>
        /// 
        /// </summary>
        int AllItemsCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        int CurrentPage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        int PerPageCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        int PagesCount
        {
            get;
        }
    }
}
