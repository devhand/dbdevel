﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBDevel.Core
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PagingList<T> : List<T>, IPagingList
    {
        /// <summary>
        /// 
        /// </summary>
        public PagingList()
        {
            this.CurrentPage = 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="collection"></param>
        public PagingList(IEnumerable<T> collection)
            : base(collection)
        {
            this.CurrentPage = 1;
        }

        /// <summary>
        /// 
        /// </summary>
        public int AllItemsCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PerPageCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PagesCount
        {
            get
            {
                return (int)Math.Ceiling((double)this.AllItemsCount / (double)this.PerPageCount);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int AllItemsCountNoFilters { get; set; }
    }
}
